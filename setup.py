"""splittable_kwargs: split **kwargs among several functions.

Nobody likes maintaining keyword argument defaults across several
levels of nested functions.  Now you don't have to, just use **kwargs
for all your child functions, and let splittable_kwargs manage the
partitioning for you.
"""

from distutils.core import setup

from splittable_kwargs import __version__


classifiers = """\
Development Status :: 2 - Pre-Alpha
Intended Audience :: Developers
Operating System :: POSIX
Operating System :: Unix
License :: Public Domain
Programming Language :: Python
Topic :: Software Development :: Libraries :: Python Modules
"""

doclines = __doc__.split("\n")


setup(name="splittable_kwargs",
      version=__version__,
      maintainer="W. Trevor King",
      maintainer_email="wking@drexel.edu",
      url = "http://www.physics.drexel.edu/~wking/unfolding-disasters/posts/splittable_kwargs/",
      download_url = "http://www.physics.drexel.edu/~wking/code/python/splittable_kwargs-%s.tar.gz"
      % __version__,
      license = "Public Domain",
      platforms = ["all"],
      description = doclines[0],
      long_description = "\n".join(doclines[2:]),
      classifiers = filter(None, classifiers.split("\n")),
      py_modules = ['splittable_kwargs'],
      )
