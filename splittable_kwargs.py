# Copyright (C)  W. Trevor King  2008, 2009, 2010
# This code is released to the public domain.

"""Split `**kwargs` arguments among several functions.

Example usage (adapted from the unittests)

    >>> from splittable_kwargs import *
    >>> @splittableKwargsFunction()
    ... def foo(x, y, z=2):
    ...     return "foo: x "+str(x)+", y "+str(y)+", z "+str(z)+"\\n"
    ... 
    >>> @splittableKwargsFunction()
    ... def bar(a, b, c=2):
    ...     return "bar: a "+str(a)+", b "+str(b)+", c "+str(c)+"\\n"
    >>> @splittableKwargsFunction({'fn':bar, 'mask':'a', 'translate':{'b':'b_bar'}})
    ... def baz(d, **kwargs):
    ...     barkw, = baz._splitargs(baz, kwargs) # for translation/masking
    ...     string = bar(a=5, c=4, **barkw)
    ...     return string + "baz: d "+str(d)+"\\n"
    >>> @splittableKwargsFunction(foo, baz)
    ... def simple_joint(**kwargs):
    ...     fookw,bazkw = simple_joint._splitargs(simple_joint, kwargs)
    ...     string = foo(**fookw)
    ...     string += baz(**bazkw)
    ...     return string.strip()
    >>> print simple_joint(x=1,y=3,b_bar=6,d=7)
    foo: x 1, y 3, z 2
    bar: a 5, b 6, c 4
    baz: d 7

If, say, simple_joint's children had not been defined (you wanted to
define baz after simple_joint in your module), you can skip the
decorator in simple_joint, and make it splittable later (after you
define baz) with

   make_splittable_kwargs_function(simple_joint, foo, baz)

You can also get a list of the available named arguments with

    >>> print baz._kwargs(baz)
    ['d', 'b_bar', 'c']
    >>> simple_joint._kwargs(simple_joint)
    ['x', 'y', 'z', 'd', 'b_bar', 'c']

It may seem redundant to need to pass the function (here simple_joint)
to a method of simple_joint, but remember that simple_joint is a
_function_, not a class instance.  If it really bothers you, try
something like

    >>> class ClassJoint (object):
    ...     @splittableKwargsFunction(foo, baz)
    ...     def __call__(self, **kwargs):
    ...         fookw,bazkw = simple_joint._splitargs(simple_joint, kwargs)
    ...         string = foo(**fookw)
    ...         string += baz(**bazkw)
    ...         return string.strip()
    ...     def _kwargs(self):
    ...         return self.__call__._kwargs(self.__call__)
    >>> cj = ClassJoint()
    >>> cj._kwargs()
    ['self', 'x', 'y', 'z', 'd', 'b_bar', 'c']
    >>> print cj(x=1,y=3,b_bar=6,d=7)
    foo: x 1, y 3, z 2
    bar: a 5, b 6, c 4
    baz: d 7
"""

import inspect
import doctest
import unittest


__version__ = '0.4'


class UnknownKwarg (KeyError):
    def __init__(self, fn, kwarg, value):
        if hasattr(fn, "_kwargs"):
            fn_list = [fn]
        else:
            fn_list = fn
        msg = "Unknown kwarg %s = %s.  Allowed:\n" \
            % (kwarg, value)
        for f in fn_list:
            msg += "  %s %s\n" % (f.__name__, f._kwargs(f))
        KeyError.__init__(self, msg)


def _parse_splittable(splittable):
    """
      splittable -> (splittable_fn, masked_args, translated_args)
    """
    if hasattr(splittable, "_kwargs"): # bare splittableKwargsFunction
        return (splittable, [], {})
    else: # masked/translated dict
        return (splittable['fn'],
                splittable.get('mask', []),
                splittable.get('translate', {}))

def splitargs(kwargs, *internal_splittables):
    """
    where
      *internal_splittables : a list of splittableKwargsFunctions
          items that this function uses internally.  The items must be
          parsable by _parse_splittable().
    """
    # sort the kwargs according to the appropriate function
    fn_kwargs = [{} for splittable in internal_splittables]
    for key,value in kwargs.items():
        sorted = False
        for i,splittable in enumerate(internal_splittables):
            fn,masked,translated = _parse_splittable(splittable)
            inv_translation = dict([(v,k) for k,v in translated.items()])
            if key in inv_translation:
                child_key = inv_translation[key]
                sorted = True
            elif key in fn._kwargs(fn) and key not in masked:
                child_key = key
                sorted = True
            if sorted == True:
                fn_kwargs[i][child_key] = value
                break
        if sorted != True: # couldn't find anywhere for that key.
            fns = []
            for splittable in internal_splittables:
                fn,masked,translated = _parse_splittable(splittable)
                fns.append(fn)
            raise UnknownKwarg(fns, key, value)
    return fn_kwargs

def _kwargs(self):
    "Return a list of acceptable kwargs"
    args,varargs,varkw,defaults = inspect.getargspec(self)
    if varargs != None:
        raise NotImplementedError, "\n %s" % varargs
    if varkw != None:
        for child in self._childSplittables:
            child,masked,translated = _parse_splittable(child)
            child_args = child._kwargs(child)
            for arg in masked:
                try:
                    child_args.remove(arg)
                except ValueError, e:
                    msg = "%s not in %s" % (arg, child_args)
                    raise ValueError(msg)
            for original,new in translated.items():
                try:
                    child_args[child_args.index(original)] = new
                except ValueError, e:
                    msg = "%s not in %s" % (arg, child_args)
                    raise ValueError(msg)
            args.extend(child_args)
    return args

def _declareInternalSplittableKwargsFunction(self, function):
    """
    FUNCTION can be either a bare splittableKwargsFuntion or a dict of
    the form:
      {'fn': splittableKwargsFuntion,
       'mask':["argumentA",...],
       'translate':{"argumentB":"argB_new_name", ...}}
    
    Example values for FUNCTION:
      bar
      {'fn':bar, mask:["a"]}
      {'fn':bar, mask:["a", "b"], translate:{"c":"bar_c"}}}
    """
    self._childSplittables.append(function)

def make_splittable_kwargs_function(function, *internal_splittables):
    function._kwargs = _kwargs
    function._childSplittables = []
    function._declareInternalSplittableKwargsFunction = \
             _declareInternalSplittableKwargsFunction
    def _splitargs(self, **kwargs):
        return splitargs(kwargs, *self._childSplittables)
    function._splitargs = lambda self,kwargs : splitargs(kwargs, *self._childSplittables)
    
    for splittable in internal_splittables:
        function._declareInternalSplittableKwargsFunction(function, splittable)


class splittableKwargsFunction (object):
    def __init__(self, *internal_splittables):
        self.internal_splittables = internal_splittables
    def __call__(self, function):
        make_splittable_kwargs_function(function, *self.internal_splittables)
        return function


@splittableKwargsFunction()
def _foo(x, y, z=2):
    "foo function"
    return "foo: x "+str(x)+", y "+str(y)+", z "+str(z)+"\n"

@splittableKwargsFunction()
def _bar(a, b, c=2):
    "bar function"
    return "bar: a "+str(a)+", b "+str(b)+", c "+str(c)+"\n"

@splittableKwargsFunction({'fn':_bar, 'mask':'a', 'translate':{'b':'b_bar'}})
def _baz(d, **kwargs):
    "baz function"
    barkw, = _baz._splitargs(_baz, kwargs) # for translation/masking
    string = _bar(a=6, c=4, **barkw)
    return string + "baz: d "+str(d)+"\n"

@splittableKwargsFunction(_foo, _bar)
def _fun(**kwargs):
    fookw,barkw = _fun._splitargs(_fun, kwargs)
    return(fookw, barkw)

class SplittableKwargsTestCase (unittest.TestCase):
    @splittableKwargsFunction(_foo, _bar)
    def foobar(self, **kwargs):
        fookw,barkw = self.foobar._splitargs(self.foobar, kwargs)
        string = _foo(**fookw)
        string += _bar(**barkw)
        return string

    @staticmethod
    def simple_joint(**kwargs):
        string = "joining\n"
        keys = kwargs.keys()
        keys.sort()
        string += "\n".join(["  %s : %s" % (k,kwargs[k]) for k in keys])
        string += "\n"
        fookw,barkw = splitargs(kwargs, _foo, _bar)
        string += _foo(**fookw)
        string += _bar(a=4, **barkw)
        return string

    @staticmethod
    def deeper_joint(**kwargs):
        string = "joining\n"
        keys = kwargs.keys()
        keys.sort()
        string += "\n".join(["  %s : %s" % (k,kwargs[k]) for k in keys])
        string += "\n"
        fookw,bazkw = splitargs(kwargs, _foo, _baz)
        string += _foo(**fookw)
        string += _baz(**bazkw)
        return string

    def testDocPreserved(self):
        self.failUnless(_foo.__doc__ == "foo function", _foo.__doc__)
        self.failUnless(_bar.__doc__ == "bar function", _bar.__doc__)
        self.failUnless(_baz.__doc__ == "baz function", _baz.__doc__)
    def testSingleLevelMethod(self):
        expected = """foo: x 1, y 2, z 3
bar: a 8, b 5, c 2
"""
        output = self.foobar(x=1, y=2, z=3, a=8, b=5)
        self.failUnless(output == expected,
                        "GOT\n%s\nEXPECTED\n%s" % (output, expected))
    def testSingleLevelFunction(self):
        simple_joint = SplittableKwargsTestCase.simple_joint
        expected = """joining
  b : 5
  x : 1
  y : 2
  z : 3
foo: x 1, y 2, z 3
bar: a 4, b 5, c 2
"""
        output = simple_joint(x=1, y=2, z=3, b=5)
        self.failUnless(output == expected,
                        "GOT\n%s\nEXPECTED\n%s" % (output, expected))
    def testSingleLevelUnknownKwarg(self):
        simple_joint = SplittableKwargsTestCase.simple_joint
        self.assertRaises(UnknownKwarg, simple_joint,
                          y=2, z=3, b=5, unknown=6)
    def testDoubleLevel(self):
        deeper_joint = SplittableKwargsTestCase.deeper_joint
        expected = """joining
  b_bar : 5
  d : 6
  x : 1
  y : 2
  z : 3
foo: x 1, y 2, z 3
bar: a 6, b 5, c 4
baz: d 6
"""
        output = deeper_joint(x=1, y=2, z=3, b_bar=5, d=6)
        self.failUnless(output == expected,
                        "GOT\n%s\nEXPECTED\n%s" % (output, expected))
    def testDoubleLevelOverrideRequired(self):
        deeper_joint = SplittableKwargsTestCase.deeper_joint
        self.assertRaises(UnknownKwarg, deeper_joint,
                          x=8, y=2, z=3, b=5, a=1)
    def testRecursiveSplitargsReference(self):
        # Access the ._splitargs method using the defined function name
        expected = ({'y':3, 'z':4}, {'a':1, 'b':2})
        output = _fun(a=1,b=2,y=3,z=4)
        self.failUnless(output == expected,
                        "GOT\n%s\nEXPECTED\n%s" % (str(output), str(expected)))


if __name__ == "__main__":
    import sys
    
    unitsuite = unittest.TestLoader().loadTestsFromTestCase(
        SplittableKwargsTestCase)
    unitsuite.addTest(doctest.DocTestSuite())
    result = unittest.TextTestRunner(verbosity=2).run(unitsuite)
    numErrors = len(result.errors)
    numFailures = len(result.failures)
    numBad = numErrors + numFailures
    if numBad > 126:
        numBad = 1
    sys.exit(numBad)
