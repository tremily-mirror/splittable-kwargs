# Problem: how to split **kwargs among several functions?
# Which args should go to which functions?
#
# The parsing code is inspired by and developed from Prashanth Ellina's
#   http://blog.prashanthellina.com/2007/11/14/generating-call-graphs-for-understanding-and-refactoring-python-code/
#
# See
#   http://docs.python.org/reference/grammar.html
# for the Python grammar

import parser
import symbol
import token
import inspect

import pprint
import copy

def get_parse_item_code(parse_item):
    if isinstance(parse_item, list):
        item_code = parse_item[0]
    else:
        item_code = parse_item
    return item_code

def code_to_string(parse_code):
    if parse_code in symbol.sym_name:
        code_string = symbol.sym_name[parse_code]
    else:
        code_string = token.tok_name[parse_code]
    return code_string

def annotate_parse_list(parse_list, toplevel=True):
    if toplevel == True:
        parse_list = copy.deepcopy(parse_list)
    parse_list[0] = code_to_string(parse_list[0])

    for index, item in enumerate(parse_list):
        if index == 0: continue
        if isinstance(item, list):
            parse_list[index] = annotate_parse_list(item, toplevel=False)
    return parse_list

def match_item_code(parse_item, expected_code):
    parse_code = get_parse_item_code(parse_item)
    return parse_code == expected_code

def assert_item_code(parse_item, expected_code):
    if not match_item_code(parse_item, expected_code):
        raise Exception, "Unexpected code %s != %s in\n%s" \
            % (code_to_string(parse_code),
               code_to_string(expected_code),
               pprint.pformat(annotate_parse_list(parse_item)))

def drill_for_item_code(parse_item, target_code):
    """Depth first search for the target_code"""
    parse_code = get_parse_item_code(parse_item)
    if parse_code == target_code:
        return parse_item
    if isinstance(parse_item, list):
        for child_item in parse_item[1:]:
            ret = drill_for_item_code(child_item, target_code)
            if ret != None:
                return ret
    return None

def parse_atom(atom):
    """
    atom: ('(' [yield_expr|testlist_gexp] ')' |
       '[' [listmaker] ']' |
       '{' [dictmaker] '}' |
       '`' testlist1 '`' |
       NAME | NUMBER | STRING+)
    """
    assert_item_code(atom, symbol.atom)
    first_child = atom[1]
    first_child_code = first_child[0]
    if first_child_code == token.NAME:
        return first_child[1]
    elif first_child_code == token.NUMBER:
        return first_child[1]
    elif first_child_code == token.STRING:
        return first_child[1]
    return None

def find_test_name(parse_item):
    """
    Drill down to the atom naming this test item
    """
    assert_item_code(parse_item, symbol.test)
    atom_item = drill_for_item_code(parse_item, symbol.atom)
    if atom_item == None:
        return None
    return parse_atom(atom_item)

def parse_argument(parse_item):
    "argument: test [gen_for] | test '=' test  # Really [keyword '='] test"
    assert_item_code(parse_item, symbol.argument)
    if len(parse_item) == 4: # test '=' test
        arg_name = find_test_name(parse_item[1])
        assert_item_code(parse_item[2], token.EQUAL)
        arg_value = find_test_name(parse_item[3])
    elif len(parse_item) == 2: # test
        arg_name = None
        arg_value = find_test_name(parse_item[1])
    else: # test gen_for
        raise NotImplementedError, \
            '"test gen_for" argument\n%s' \
            % pprint.pformat(annotate_parse_list(parse_item))
    return (arg_name, arg_value)

def parse_arglist(parse_item):
    """
    arglist: (argument ',')* (argument [',']
                         |'*' test (',' argument)* [',' '**' test] 
                         |'**' test)
    """
    assert_item_code(parse_item, symbol.arglist)
    args = []
    kwargs = {}
    varargs = None
    varkw = None
    i = 1
    while i < len(parse_item):
        item = parse_item[i]
        argument_code = get_parse_item_code(item)
        if argument_code == token.COMMA:
            pass
        elif argument_code == symbol.argument:
            arg_name,arg_value = parse_argument(item)
            if arg_name == None:
                args.append(arg_value)
            else:
                kwargs[arg_name] = arg_value
        elif argument_code == token.DOUBLESTAR:
            i += 1
            item = parse_item[i]
            varkw = find_test_name(item)
        else:
            raise NotImplementedError, \
                '"Unknown arglist item %s in\n%s' \
                % (code_to_string(argument_code),
                   pprint.pformat(annotate_parse_list(parse_item)))
        i += 1
    return (args, kwargs, varargs, varkw)

def parse_trailer(parse_item):
    """
    trailer: '(' [arglist] ')' | '[' subscriptlist ']' | '.' NAME
    """
    assert_item_code(parse_item, symbol.trailer)
    if match_item_code(parse_item[1], token.LPAR):
        assert_item_code(parse_item[-1], token.RPAR)
        if len(parse_item) == 4: # '(' arglist ')'
            return parse_arglist(parse_item[2])
        else: # '(' ')'
            assert len(parse_item) == 3, \
                pprint.pformat(annotate_parse_list(parse_item))
            return None
    # Ignore other trailers (they can't contain arglists)
    return None

def parse_power_fn(parse_item):
    """
    power: atom trailer* ['**' factor]
    """
    assert_item_code(parse_item, symbol.power)
    if len(parse_item) < 3: # "atom"
        return None
    fn_name = parse_atom(parse_item[1])
    arglists = []
    for item in parse_item[2:]:
        if match_item_code(item, symbol.trailer):
            ret = parse_trailer(item)
            if ret != None:
                arglists.append(ret)
        # ignore the possible "'**' factor"
    return (fn_name, arglists)

def find_fn_calls(parse_item):
    calls = []
    if match_item_code(parse_item, symbol.power):
        ret = parse_power_fn(parse_item)
        if ret != None:
            fn_name,arglists = ret
            if fn_name not in dir(__builtins__):
                calls.append(ret)

    for item in parse_item[1:]:
        if isinstance(item, list):
            calls.extend(find_fn_calls(item))
    return calls

def get_called_functions(fn, witharg=None):
    """
    Return a list of functions called by the function FN if they are
    passed an argument WITHARG.
    """
    called_fns = []
    source = inspect.getsource(fn)
    suite = parser.suite(source)
    parse_list = parser.st2list(suite)
    #annotated_parse_list = annotate_parse_list(parse_list)
    #pprint.pprint(annotated_parse_list)
    calls = find_fn_calls(parse_list)
    for fn_name,arglists in calls:
        for i,arglist in enumerate(arglists):
            args,kwargs,varargs,varkw = arglist
            has_witharg = False
            if witharg in kwargs:
                has_witharg = True
            elif varargs != None and witharg == "*"+varargs:
                has_witharg = True
            elif varkw != None and witharg == "**"+varkw:
                has_witharg = True
            if has_witharg:
                assert i == 0, \
                    "Don't know function name (stacked arglists)\n%s %s\n\n%s"\
                    % (fn_name, arglists, source)
                if fn_name not in called_fns:
                    called_fns.append(fn_name)
    return called_fns

def get_fn_by_name(fn_name):
    if fn_name in globals():
        fn_instance = globals()[fn_name]
    else:
        raise Exception, "No source for function %s" % fn_name
    return fn_instance

def get_fn_args(fn, recurse=True, checked_fns=None):
    """
    Return a list of argument names accepted by FN.  If RECURSE ==
    True and FN takes a **kwargs style argument, then attempt to drill
    down into the children functions called inside FN to find what
    argument names that **kwargs argument will accept.
    
    The recursive drilling is quite cludgy, and currently only supports
    child functions where **kwargs is used directly.  E.g. if you manually
    pop a value from kwargs, and pass the popped value on to a child, this
    function will not notice.
    """
    if checked_fns == None:
        checked_fns = [fn]
    print "get args for %s (checked %s)" % (fn, checked_fns)
    args,varargs,varkw,defaults = inspect.getargspec(fn)
    if varargs != None:
       raise NotImplementedError, "\n %s" % varargs
    if varkw != None and recurse == True:
        # This step is probably not worth the trouble
        children = get_called_functions(fn, witharg='**'+varkw)
        for f in children:
            f_instance = get_fn_by_name(f)
            args.extend(get_fn_args(f_instance, checked_fns=checked_fns))
    return args

def splitargs(fn_list, kwargs):
    # get list of allowed arguments
    fn_args = []
    for fn in fn_list:
        fn_args.append(get_fn_args(fn))

    # sort the kwargs according to the appropriate function
    fn_kwargs = [{} for fn in fn_list]
    for key,value in kwargs.items():
        sorted = False
        for i,fn,args in zip(range(len(fn_list)), fn_list, fn_args):
            if key in args:
                fn_kwargs[i][key] = value
                sorted = True
                break
        if sorted != True:
            raise Exception, "Unrecognized argument %s = %s" % (key, value)
    return fn_kwargs


def foo(x, y, z=2):
    print "foo: x", x, "y", y, "z", z

def bar(a, b, c=2):
    print "bar: a", a, "b", b, "c", c

def baz(d, **kwargs):
    bar(c=4, **kwargs)
    print "baz: d", d

def simple_joint(**kwargs):
    print "joining"
    print '\n'.join(["  %s : %s" % (k,v) for k,v in kwargs.items()])
    fookw,barkw = splitargs([foo,bar], kwargs)
    foo(**fookw)
    bar(**barkw)
    print ""

def deeper_joint(**kwargs):
    print "joining"
    print '\n'.join(["  %s : %s" % (k,v) for k,v in kwargs.items()])
    fookw,bazkw = splitargs([foo,baz], kwargs)
    foo(**fookw)
    baz(**bazkw)
    print ""


# A single level is easily handled, since the list of arguments can be
# read off the output of inspect.getargspec().
simple_joint(x=1, y=2, z=3, a=4, b=5)
try:
    simple_joint(x=1, y=2, z=3, a=4, b=5, unknown=5)
except Exception, e:
    print e

# This is much more complicated.  With multiple levels of **kwargs to
# drill down, we need to parse the definition of each function to see
# what arguments the first **kwargs can take.
deeper_joint(x=1, y=2, z=3, a=4, b=5, d=6)
