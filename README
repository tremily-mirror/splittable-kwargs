Problem: how to split **kwargs among several functions?
Which args should go to which functions?
For example:

  def foo(x, y, z=2):
      print "foo: x", x, "y", y, "z", z
  def bar(a, b, c=2):
      print "bar: a", a, "b", b, "c", c
  def baz(d, **kwargs):
      bar(c=4, **kwargs)
      print "baz: d", d
  def simple_joint(**kwargs):
      print "joining"
      print '\n'.join(["  %s : %s" % (k,v) for k,v in kwargs.items()])
      fookw,barkw = splitargs([foo,bar], kwargs)
      foo(**fookw)
      bar(**barkw)
      print ""
  def deeper_joint(**kwargs):
      print "joining"
      print '\n'.join(["  %s : %s" % (k,v) for k,v in kwargs.items()])
      fookw,bazkw = splitargs([foo,baz], kwargs)
      foo(**fookw)
      baz(**bazkw)
      print ""
  
  # A single level is easily handled, since the list of arguments can be
  # read off the output of inspect.getargspec().
  simple_joint(x=1, y=2, z=3, a=4, b=5)
  try:
      simple_joint(x=1, y=2, z=3, a=4, b=5, unknown=5)
  except Exception, e:
      print e
  
  # This is much more complicated.  With multiple levels of **kwargs to
  # drill down, we need to parse the definition of each function to see
  # what arguments the first **kwargs can take.
  deeper_joint(x=1, y=2, z=3, a=4, b=5, d=6)

A first attempt at a solution (which works fairly well, but is a bit
of a brain-melter) is given in parse.py.  A much simpler and more
robust solution is given in splittable_kwargs.py.
