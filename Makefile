.PHONY : all check dist clean

all : dummy_py

dummy_py : setup.py splittable_kwargs.py
	python setup.py install --home=~
	echo "dummy for Makefile dependencies" > $@

check : all
	python splittable_kwargs.py

dist :
	python setup.py sdist
	scp dist/splittable_kwargs*tar.gz einstein:public_html/code/python/

clean :
	python setup.py clean
	rm -rf build dist splittable_kwargs.egg-info
	rm -f dummy_py *.pyc
